
val id : 'a1 -> 'a1

val eqb : Uint63.t -> Uint63.t -> bool

val leb : Uint63.t -> Uint63.t -> bool

val merge : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list -> 'a1 list

val merge_sort_pop :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list list -> 'a1 list

val merge_sort_push :
  ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list list -> 'a1 list list

val merge_sort_rec :
  ('a1 -> 'a1 -> bool) -> 'a1 list list -> 'a1 list -> 'a1 list

val sort : ('a1 -> 'a1 -> bool) -> 'a1 list -> 'a1 list

val inintlist : Uint63.t -> Uint63.t list -> bool

val append1_and_sort : Uint63.t list -> Uint63.t -> Uint63.t list

val append1_sorted : Uint63.t list -> Uint63.t -> Uint63.t list
