
type __ = Obj.t

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

type reflect =
| ReflectT
| ReflectF

val iffP : bool -> reflect -> reflect

val idP : bool -> reflect

type 't pred = 't -> bool

type 't predType =
  __ -> 't pred
  (* singleton inductive, whose constructor was PredType *)

type 't pred_sort = __

type 't rel = 't -> 't pred

type 't mem_pred = 't pred
  (* singleton inductive, whose constructor was Mem *)

val pred_of_mem : 'a1 mem_pred -> 'a1 pred_sort

val in_mem : 'a1 -> 'a1 mem_pred -> bool

val mem : 'a1 predType -> 'a1 pred_sort -> 'a1 mem_pred

val predType0 : ('a2 -> 'a1 pred) -> 'a1 predType

module Equality :
 sig
  type 't axiom = 't -> 't -> reflect

  type 't mixin_of = { op : 't rel; mixin_of__1 : 't axiom }

  val op : 'a1 mixin_of -> 'a1 rel

  type coq_type =
    __ mixin_of
    (* singleton inductive, whose constructor was Pack *)

  type sort = __

  val coq_class : coq_type -> sort mixin_of
 end

val eq_op : Equality.coq_type -> Equality.sort rel

type 't subType = { val0 : (__ -> 't); sub : ('t -> __ -> __);
                    subType__2 : (__ -> ('t -> __ -> __) -> __ -> __) }

val sig_subType : 'a1 pred -> 'a1 subType

val mem_seq : Equality.coq_type -> Equality.sort list -> Equality.sort -> bool

type seq_eqclass = Equality.sort list

val pred_of_seq : Equality.coq_type -> seq_eqclass -> Equality.sort pred_sort

val seq_predType : Equality.coq_type -> Equality.sort predType

val path : 'a1 rel -> 'a1 -> 'a1 list -> bool

val sorted : 'a1 rel -> 'a1 list -> bool

val merge : 'a1 rel -> 'a1 list -> 'a1 list -> 'a1 list

val merge_sort_push : 'a1 rel -> 'a1 list -> 'a1 list list -> 'a1 list list

val merge_sort_pop : 'a1 rel -> 'a1 list -> 'a1 list list -> 'a1 list

val merge_sort_rec : 'a1 rel -> 'a1 list list -> 'a1 list -> 'a1 list

val sort0 : 'a1 rel -> 'a1 list -> 'a1 list

val eqb : Uint63.t -> Uint63.t -> bool

val leb : Uint63.t -> Uint63.t -> bool

val eqbP : Uint63.t Equality.axiom

val uint_eqMixin : Uint63.t Equality.mixin_of

val uint_eqType : Equality.coq_type

val append1_and_sort : Uint63.t list -> Uint63.t -> Uint63.t list

val append1_sorted : Uint63.t list -> Uint63.t -> Uint63.t list
