\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[top=3cm, bottom=3cm, left=3cm, right=2.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{xspace}
\usepackage{tabu}
\usepackage{mdframed}
\usepackage{url}
\usepackage{chngcntr}
\usepackage{verbatim}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage[dvipsnames]{xcolor} % fancier colors
\usepackage{bm} % Improved bold face in math mode
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\setlength{\parskip}{4pt}



\usepackage{listings}
\include{Programming}
\usepackage{tcolorbox}
\tcbuselibrary{listings,skins}
\newtcblisting{coqc}[2][hbox,enhanced,drop shadow,center]{
  arc=0pt, outer arc=4pt,
  listing only,
  listing style=coqq,
  title=#2,
  #1
}

\title{A code-writing practice for a clean extraction of Coq code}
\author{Mireia González Bedmar \and Ana de Almeida Borges \and Joost J. Joosten}
\date{Formal Vindications S.L.\\Universitat de Barcelona\\ \vspace{10mm}December 17, 2021}


\begin{document}

\maketitle

\begin{abstract}
  The goal of this document is to describe a method
  for extracting
  programs that have been verified from Coq, in such a way that the resulting
  OCaml code is clean, reasonably short, and readable. To this end, we present
  a very simple example of Coq program where we have applied this method.

  In the first section we present a description of the methodology, which
  is actually just a general idea that can be probably applied in several ways.
  This section should be enough for grasping the general spirit of the method.
  The next section presents an example, with simple code that has been
  written for this purpose.
\end{abstract}

\textbf{Author roles.} Mireia González Bedmar: Conceptualization, Methodology, Software, Writing – original draft; Ana de Almeida Borges: Conceptualization, Methodology, Writing – review \& editing; Joost J. Joosten: Funding acquisition, Project administration.

\tableofcontents

\newpage
\section{Description of the procedure}
We assume that we have a Coq function that we wish to extract to OCaml.
In our use cases, Coq functions that are meant to be extracted do not use
dependent types or other features that OCaml cannot represent, because
we purposefully write the code meant to be executed in this way.
However, the method we present here can be easily adapted to a situation
where dependent types are used, at least when the condition on the type
is computable, and here we also describe how, even though we still think
that the best practice is to avoid dependent types on the implementation side.

What we do use are MathComp structures, even in code that represents the
implementation part of our verified software. Extracting those structures
to OCaml yields unreadable and unfeasibly long code files, which is why we have
developed this method. It's a way of keeping the power of MathComp for
implementing and proving correctness, and still getting feasible code when
performing extraction.

\paragraph{Procedure}

We have a collection of Coq functions that we wish to extract.
Assume they are in a file named \texttt{original.v}.
As discussed above, these functions may use the MathComp library
(for example, an \texttt{eqType}), and certain dependent types.
\begin{enumerate}
  \item In a new file (let's name it \texttt{extraction\_file.v})
    which does not import nor depend on any other
    file of our development, and which only imports the most basic parts
    of Coq libraries (such as \texttt{List}),
    we rewrite all the functions that are meant
    to be extracted, recursively -- i.e., we rewrite the functions that
    our functions use, whether they are written by us or provided by
    MathComp or others. This time, we do not use MathComp structures or
    dependent types -- we write the code ``plain-style''. Find several
    instances of this simplification on the example below.
  \item In this file we import the \texttt{Extraction} tools necessary and
    write the extraction command for the functions we wish.
  \item We write another file (name it \texttt{extraction\_file\_correct.v})
    which imports both the previous one, \texttt{extraction\_file}, and
    all of our development plus auxiliary libraries that we use. In this
    file we write a bunch of lemmas of the form:
\end{enumerate}
\begin{coqc}{Correctness for the rewritten version of \texttt{function\_name}}
Lemma function_nameE (arguments) :
  function_name arguments = original.function_name arguments.
\end{coqc}

\paragraph{Advantages}
\begin{itemize}
\item Total control over what is extracted, including better control over
  the naming of the functions and types.
\item No \texttt{Obj.magic} in the extracted code.
\item Extracted code is shorter and more readable.
\item Some error control can be included in the version for extraction
  (see below).
\end{itemize}

\newpage
\section{Example}

In our example\footnote{The code can be found in
  \url{https://gitlab.com/formal-vindications/extraction-model}}
implementation file, named \texttt{original.v},
we have two functions implemented in Coq using features that are not
recommended for extraction: both use MathComp tools, and one of them also
uses a dependent type. Let's take a look.
\begin{coqc}{\texttt{append1\_and\_sort}}
Definition append1_and_sort (s : seq int) (x : int) :=
  if x \in s then sort Uint63.leb s
  else sort Uint63.leb (x :: s).
\end{coqc}

This function takes a list \texttt{s} and an element \texttt{x}, appends the
element to the list in case it is not already in it, and sorts the result
using the MathComp sorting function.

Similarly, to show an example with dependent types, we can write a function
that does the same but assuming that the input list is already sorted:

\begin{coqc}{\texttt{append1\_sorted}}
Definition append1_sorted (s : {r : seq int | sorted Uint63.leb r}) (x : int) :=
  if x \in val s then val s
  else merge Uint63.leb [:: x] (val s).
\end{coqc}

In a real-world case, we would have in \texttt{original.v} or other files
of our development,
many lemmas proving mathematical properties about these functions.

The result of extracting these functions naively, i.e., as in file
\texttt{bad\_extraction.v}, can be seen at the file \texttt{bad\_extraction.ml}.
For two simple functions as the ones we have, this code contains several
instances of \texttt{Obj.magic}, is quite unreadable and
twice as long as the alternative that
we present -- although the length is not critical here,
we have seen in more complex use cases that
it can easily get to an unfeasible point.

Now let's visit the steps of our method applied to this example.

\subsection{Rewriting the code, plain-style}
In \texttt{extraction\_file.v}, we rewrite the code avoiding the use of
problematic structures. The header of this file only has:
\begin{coqc}{Header of \texttt{extraction\_file.v}}
Require Import List Uint63.
Require Extraction ExtrOcamlBasic ExtrOCamlInt63.
Import ListNotations.
\end{coqc}

No SSReflect/MathComp, and no files of our development (\texttt{original.v}).
The rewriting of the first function goes as follows:
\begin{coqc}{\texttt{append1\_and\_sort} plain-style}
Definition append1_and_sort (s : list int) (x : int) :=
  if inintlist x s then sort Uint63.leb s
  else sort Uint63.leb (x :: s).
\end{coqc}

To get rid of the MathComp expression \texttt{x \symbol{92}in s},
we have defined a
specialized function that checks if an \texttt{int} is in a \texttt{list int}.
This function looks as follows:
\begin{coqc}{\texttt{inintlist}}
Fixpoint inintlist (x : int) (l : list int) : bool :=
  match l with
  | [] => false
  | y :: l' => if (x =? y)%uint63 then true else inintlist x l'
  end.
\end{coqc}

Instead of relying on the MathComp \texttt{eqType} machinery, we use directly
the boolean equality for the type \texttt{Uint63.int}.

Similarly, notice that in this definition, \texttt{sort} is not the MathComp
function (MathComp is not imported!), it's a rewriting of it.

The plain-style version of \texttt{append1\_sorted} might be more debatable.
Here we offer two possible choices, depending on
the needs that the verified code is supposed to fulfill.

The first one consists in getting rid of the dependent type of the argument,
and just assuming that the user will provide a correct input.

\begin{coqc}{\texttt{append1\_sorted} plain-style}
Definition append1_sorted (s : list int) (x : int) : list int :=
  if inintlist x s then s
  else merge Uint63.leb [x] s.
\end{coqc}

The second possibility also gets rid of the dependent type of the argument,
and instead includes a check that the input list is sorted. The return type
is an \texttt{option}, to give \texttt{None} in case the input is invalid.
The \texttt{option} type can be replaced by a customized return type that includes
different constructors for different kinds of errors. This way, the resulting
OCaml library expresses such errors, and if needed, a wrapper can be written
that sends those errors to exceptions.

\begin{coqc}{\texttt{append1\_sorted} plain-style with error handling}
Definition append1_sorted_option (s : list int) (x : int) : option (list int) :=
  if sorted Uint63.leb s then Some (append1_sorted s x)
  else None.
\end{coqc}

The first possibility provides better properties and use experience, but
it's not good when the extracted function is supposed to control errors.
The second possibility does control errors paying a price: compositionality is
lost.

\subsection{Extraction}
At the end of this file we write the extraction command:
\begin{coqc}{End of \texttt{extraction\_file.v}}
Extraction "extraction/extraction" append1_and_sort append1_sorted.
\end{coqc}

The result can be seen at \texttt{extraction.ml}. The code does not contain
\texttt{Obj.magic}, is readable, and as short as \texttt{extraction\_file.v}.

\paragraph{Notice:} Since this example uses Coq's \texttt{Uint63}, to test 
the extracted code you need to compile it together with
its OCaml implementation,
\texttt{uint63.ml} and \texttt{uint63.mli}, also provided in the
extraction directory.

\subsection{Correctness of the plain-style code}
The last step is proving that our rewritten code is still correct, i.e.,
it behaves the same as our \texttt{original} implementation, for which we
had many lemmas of verification.

To this end, we write the file \texttt{extraction\_file\_correct.v}, which
is mostly a list of lemmas showing that the new rewritten functions are the
same as the original ones. Although the file is quite verbose, since it requires
lemmas for each rewritten function (thus including the auxiliary ones),
all the proofs are very simple, because the functions are implemented
roughly in the same way. A good number of them can even just go by computation.

Here we include whatever we need, and of course the \texttt{original} and
\texttt{extraction\_file}. The header looks as follows:
\begin{coqc}{Header of \texttt{extraction\_file\_correct.v}}
Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect.
Set Warnings "notation-overridden, ambiguous-paths".
Require Import Uint63.
Require Import original extraction_file.
\end{coqc}

But let's focus on the lemmas for the main functions. The first one says:
\begin{coqc}{Correctness for the rewritten version of \texttt{append1\_and\_sort}}
Lemma append1_and_sortE (s : seq int) (x : int) :
  append1_and_sort s x = original.append1_and_sort s x.
\end{coqc}

No need to give explanations, since both functions behave the same.
It is a bit more convoluted in the second example, in which we
have erased a dependent type. Our correctness lemma would go:
\begin{coqc}{Correctness for the rewritten version of \texttt{append1\_sorted}}
Lemma append1_sortedE (s : {r : seq int | path.sorted Uint63.leb r}) (x : int) :
  append1_sorted (val s) x = original.append1_sorted s x.
\end{coqc}

In the version with error handling, we can prove the following:
\begin{coqc}{Correctness for the rewritten version of \texttt{append1\_sorted}}
Lemma append1_sorted_option_Some (s : {r : seq int | path.sorted Uint63.leb r})
  (x : int) :
  append1_sorted_option (val s) x = Some (original.append1_sorted s x).

Lemma append1_sorted_option_None (s : seq int) (x : int) :
  ~~ sorted Uint63.leb s -> append1_sorted_option s x = None.
\end{coqc}


The first lemma says that, when the input is an instance of the dependent type,
then the function behaves as the original one. The second lemma says that,
when the input does not satisfy the dependent type condition, the function
returns \texttt{None}.

\section{Acknowledgements}

We thank Cyril Cohen for providing the seed of this idea, and Yannick Forster
for reading a first draft and providing valuable insights.
\end{document}
