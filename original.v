Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect.
Set Warnings "notation-overridden, ambiguous-paths".
Require Import Uint63.

Lemma eqbP : Equality.axiom eqb.
Proof.
move=> x y; apply: (iffP idP)=> [|<-]; last by rewrite eqb_refl.
by move=> /eqbP/to_Z_inj ->.
Qed.

Canonical uint_eqMixin := EqMixin eqbP.
Canonical uint_eqType := Eval hnf in EqType int uint_eqMixin.

(* Appends the element x to the list s only if it is not already on the list   *)
(* and sorts the result *)
Definition append1_and_sort (s : seq int) (x : int) :=
  if x \in s then sort Uint63.leb s
  else sort Uint63.leb (x :: s).

(* Given an already sorted list s and an element x, if x is in the list,       *)
(* the result is just the list (already sorted), and otherwise, it inserts     *)
(* x in the list keeping the result sorted                                     *)
Definition append1_sorted (s : {r : seq int | sorted Uint63.leb r}) (x : int) :=
  if x \in val s then val s
  else merge Uint63.leb [:: x] (val s).

(* Here would go lemmas about these functions to prove their correctness *)
