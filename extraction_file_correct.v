Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect.
Set Warnings "notation-overridden, ambiguous-paths".
Require Import Uint63.
Require Import original extraction_file.

Lemma mergeE (T : eqType) : @merge T = @path.merge T.
Proof. by []. Qed.

Lemma merge_sort_popE (T : eqType) :
  @merge_sort_pop T = @path.merge_sort_pop T.
Proof. by []. Qed.

Lemma merge_sort_pushE (T : eqType) :
  @merge_sort_push T = @path.merge_sort_push T.
Proof. by []. Qed.

Lemma merge_sort_recE (T : eqType) :
  @merge_sort_rec T = @path.merge_sort_rec T.
Proof. by []. Qed.

Lemma sortE (T : eqType) : @sort T = @path.sort T. Proof. by []. Qed.

Lemma pathE (T : eqType) (e : rel T) (x : T) (s : seq T) :
  path e x s = path.path e x s.
Proof. by elim: s x => // a s' /= ->. Qed.

Lemma sortedE (T : eqType) (e : rel T) (s : seq T) :
  sorted e s = path.sorted e s.
Proof. by case: s => // a s' /=; rewrite pathE. Qed.

Lemma inintlistE (x : int) (l : seq int) : inintlist x l = (x \in l).
Proof. by elim: l => [// | a l /= ->]; rewrite in_cons. Qed.

Lemma append1_and_sortE (s : seq int) (x : int) :
  append1_and_sort s x = original.append1_and_sort s x.
Proof. by rewrite /append1_and_sort /original.append1_and_sort inintlistE. Qed.

Lemma append1_sortedE (s : {r : seq int | path.sorted Uint63.leb r}) (x : int) :
  append1_sorted (val s) x = original.append1_sorted s x.
Proof.
by rewrite /append1_sorted /original.append1_sorted inintlistE mergeE.
Qed.

Lemma append1_sorted_option_Some (s : {r : seq int | path.sorted Uint63.leb r})
      (x : int) :
  append1_sorted_option (val s) x = Some (original.append1_sorted s x).
Proof.
by rewrite /append1_sorted_option sortedE (proj2_sig s) append1_sortedE.
Qed.

Lemma append1_sorted_option_None (s : seq int) (x : int) :
  ~~ sorted Uint63.leb s -> append1_sorted_option s x = None.
Proof. by rewrite /append1_sorted_option; case: ifP. Qed.
