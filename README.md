# Extraction example

Extracting Coq code in a way that leads to short, clean, and readable OCaml.

## Meta

- Compatible Coq versions: 8.14 or 8.15
- Additional dependencies:
  - [Ocaml](https://ocaml.org/) 4.10 or later
  - [MathComp](https://math-comp.github.io) ssreflect 1.12 to 1.14
- Coq namespace: `ExtractionModel`
- Related publication(s):
  - [FV Time: a formally verified Coq
    library](https://arxiv.org/pdf/2209.14227.pdf) (see Appendix A)

## Installation

### Obtain dependencies

The easiest way to install the dependencies is with
[`opam`](https://opam.ocaml.org/doc/Install.html), but any other way should
work too.

1. Make sure you have Version 2 of `opam`.

2. Create a new switch for Extraction Model (or reuse the FormalV one), for
   example with the following command:
  ```
  opam switch create ExtractionModel 4.12.0
  ```
3. Add the `coq-released` repository:
  ```
  opam repo add coq-released https://coq.inria.fr/opam/released
  ```
4. Install the dependencies
  ```
  opam update && opam install coq.8.15.2 coq-mathcomp-ssreflect.1.14.0
  ```

### Build and install

From this directory:
```
make && make install
```

## HTML Documentation

To generate HTML documentation, run `make coqdoc` and point your browser at
  `docs/coqdoc/toc.html`.

The Coq documentation is also available [online][coqdoc-link], generated with
[CoqdocJS](https://github.com/palmskog/coqdocjs).


## License notices

The files [`ml/uint63.ml`](ml/uint63.ml) and [`ml/uint63.mli`](ml/uint63.mli)
are a part of the [Coq](https://github.com/coq/coq) kernel and thus under
the [LGPL-2.1](LGPL-2.1) license.

The files under [resources](resources) are part of
[coqdocjs](https://github.com/palmskog/coqdocjs) and thus under the
[BSD-2](BSD-2) license.

The file [`Makefile.common`](Makefile.common) is a modified version of
[`Makefile.common`](https://github.com/math-comp/math-comp/blob/master/mathcomp/Makefile.common),
and hence: (c) Copyright 2006-2016 Microsoft Corporation and Inria Distributed
under the terms of [CeCILL-B](CeCILL-B).
